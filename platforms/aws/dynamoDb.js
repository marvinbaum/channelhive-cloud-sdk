const DynamoDB = require('aws-sdk/clients/dynamodb');

// Chunk polyfill
if (!Array.prototype.chunk) {
    Array.prototype.chunk = function(size) {
        return this.reduce((chunks, el, i) => (i % size ?
            chunks[chunks.length - 1].push(el) :
            chunks.push([el])) && chunks, []);
    };
}
// Flatten polyfill 
if (!Array.prototype.flatten) {
    Array.prototype.flatten = function() {
        return this.reduce((acc, val) => acc.concat(val), []);
    };
}
// To Object polyfill
if (!Array.prototype.toObject) {
    Array.prototype.toObject = function(key) {
        let obj = {};
        this.forEach(entry => obj[entry[key]] = entry);
        return obj;
    };
}

module.exports = class dynamoDB {
    constructor(region) {
        this.DDB = new DynamoDB.DocumentClient({region});
    }
    get = async (TableName, Key) => this.DDB.get({TableName, Key}).promise().then(r => r.Item)
    put = async (TableName, Item, Expected) => this.DDB.put({TableName, Item, Expected}).promise()
    delete = async (TableName, Key) => this.DDB.delete({TableName, Key}).promise()
    update = async (TableName, Key, UpdateAttributes, Settings) => {
        let params = {
            TableName,
            Key
        };

        if(Settings && Settings.Expected) params.Expected = {
            ComparisonOperator: Settings.Expected.operator,
            AttributeValueList: Array.isArray(Settings.Expected.value) ? [Settings.Expected.value] : [[Settings.Expected.value]]
        }

        if(Settings && Settings.ReturnValues) params.ReturnValues = Settings.ReturnValues;

        let addOperations = [];
        let setOperations = [];
        let deleteOperations = [];
        let removeOperations = [];
        let attributeNames = {};
        let attributeValues = {}
        let conditionExpressions = [];

        Object.keys(UpdateAttributes).forEach(key => {
            if(UpdateAttributes[key] || typeof UpdateAttributes[key] == "boolean" || UpdateAttributes[key] === 0 || UpdateAttributes[key] === null) {
                if(UpdateAttributes[key] !== null && typeof UpdateAttributes[key] === "object" && UpdateAttributes[key].action) {
                    if(UpdateAttributes[key].action === "ADD") {
                    // if is nested
                        if(key.indexOf(".") >= 0) {
                        throw Error("Update operation ADD not supported for nested attributes");
                        } else {
                            addOperations.push("#" + key + "Key " + ":" + key + "Value");
                            attributeNames["#" + key + "Key"] = key;
                            attributeValues[":" + key.replace(".", "") + "Value"] = UpdateAttributes[key].value;
                        }
                    } else if (UpdateAttributes[key].action === "SET") {
                        // if is nested
                        const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                        if(key.indexOf(".") >= 0) {
                            let keyString = "";
                            const keys = key.split(".");
                            keys.forEach((keyforkeyString, index) => {
                                keyString += "#" + undottedkey + keyforkeyString + "Key";
                                if(index != keys.length - 1) {
                                    keyString += ".";
                                }
                            })
                            keyString = keyString.replace(/\-/g, "_")
                            setOperations.push(keyString + " = " + ":" + undottedkey + "Value");
                            keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"]= keyForExpression);
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key].value;
                        } else {
                            setOperations.push("#" + key + "Key = " + ":" + undottedkey + "Value");
                            attributeNames["#" + key + "Key"] = key;
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key].value;
                        }
                    } else if (UpdateAttributes[key].action === "DELETE") {
                        throw Error("Update operation DELETE not supported");
                    } else if (UpdateAttributes[key].action === "REMOVE") {
                        // if is nested
                        if(key.indexOf(".") >= 0) {
                        throw Error("Update operation REMOVE not supported for nested attributes");
                        } else {
                            removeOperations.push(key);
                        }
                    } else if (UpdateAttributes[key].action === "appendArray") {
                        // if is nested
                        const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                        if(key.indexOf(".") >= 0) {
                            let keyString = "";
                            const keys = key.split(".");
                            keys.forEach((keyforkeyString, index) => {
                                keyString += "#" + undottedkey + keyforkeyString + "Key";
                                if(index != keys.length - 1) {
                                    keyString += ".";
                                }
                            })
                            keyString = keyString.replace(/\-/g, "_")
                            setOperations.push(keyString + " = list_append(if_not_exists(" + keyString + ", :empty_list),:" + undottedkey + "Value" + ")");
                            keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"]= keyForExpression);
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key].value;
                            attributeValues[":empty_list"] = [];
                        } else {
                            setOperations.push("#" + key + "Key = list_append(if_not_exists("+key +"Key, :empty_list),:" + undottedkey + "Value" + ")");
                            attributeNames["#" + key + "Key"] = key;
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key].value;
                            attributeValues[":empty_list"] = [];
                        }
                     }else if (UpdateAttributes[key].action === "removeFromArray") {
                        // if is nested
                        const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                        if(key.indexOf(".") >= 0) {
                            let keyString = "";
                            const keys = key.split(".");
                            keys.forEach((keyforkeyString, index) => {
                                keyString += "#" + undottedkey + keyforkeyString + "Key";
                                if(index != keys.length - 1) {
                                    keyString += ".";
                                }
                            })
                            keyString = keyString.replace(/\-/g, "_")
                            removeOperations.push(keyString + "[" + UpdateAttributes[key].index + "]");
                            keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"]= keyForExpression);
                            if(UpdateAttributes[key].validations) {
                                UpdateAttributes[key].validations.forEach(validation => {
                                    if(validation.type === "equals") {
                                        conditionExpressions.push(keyString + "[" + UpdateAttributes[key].index + "]" + ".#" + validation.key + " = :" + validation.value.replace(/\-/g, "_"))
                                        attributeNames["#" + validation.key] = validation.key
                                        attributeValues[":" + validation.value.replace(/\-/g, "_")] = validation.value
                                    }
                                    
                                })
                            }
                        } else {
                            setOperations.push("#" + key + "Key = list_append(" + key + ", :" + undottedkey + "Value" + ")");
                            attributeNames["#" + key + "Key"] = key;
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key].value;
                        }
                     }
                } else {
                        const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                        if(key.indexOf(".") >= 0) {
                            let keyString = "";
                            const keys = key.split(".");
                            keys.forEach((keyforkeyString, index) => {
                                keyString += "#" + undottedkey + keyforkeyString  + "Key";
                                if(index != keys.length - 1) {
                                    keyString += ".";
                                }
                            })
                            keyString = keyString.replace(/\-/g, "_")
                            setOperations.push(keyString + " = " + ":" + undottedkey + "Value");
                            keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"] = keyForExpression)
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key]
                        } else {
                            setOperations.push("#" + key + "Key = " + ":" + undottedkey + "Value");
                            attributeNames["#" + key + "Key"] = key;
                            attributeValues[":" + undottedkey + "Value"] = UpdateAttributes[key];
                        }
                }
            }
        });
        let expressionsArr = []

        if(addOperations.length > 0) expressionsArr.push("ADD " + addOperations.join(", ")); 
        if(setOperations.length > 0) expressionsArr.push("SET " + setOperations.join(", ")); 
        if(deleteOperations.length > 0) expressionsArr.push("DELETE " + deleteOperations.join(", ")); 
        if(removeOperations.length > 0) expressionsArr.push("REMOVE " + removeOperations.join(", "));
        if(conditionExpressions.length > 0) params.ConditionExpression = conditionExpressions.join(" AND ")
        params.UpdateExpression = expressionsArr.join(" ");
        params.ExpressionAttributeNames = attributeNames;
        params.ExpressionAttributeValues = attributeValues;
        // console.log("addOperations", addOperations);
        console.log("setOperations", setOperations);
        // console.log("deleteOperations", deleteOperations);
        // console.log("removeOperations", removeOperations);
        console.log("attributeNames", attributeNames);
        console.log("attributeValues", attributeValues);
        // console.log("expression", expressionsArr.join(" "))
        //console.log(params)
        return this.DDB.update(params).promise();

    }
    batchGet = async (TableName, Keys) => (await Promise.all(Keys.chunk(25).map(Keys => this.DDB.batchGet({
        RequestItems: {[TableName]: {Keys}}}).promise().then(r => r.Responses[TableName])
    ))).flatten()
    batchPut = async (TableName, Items) => await Promise.all(Items.chunk(25).map(chunk => this.DDB.batchWrite({
        RequestItems: {[TableName]: chunk.map(Item => ({PutRequest: {Item}}))}}).promise()
    ))
    batchPutSecure = async (TableName, Items, uniqueKey, maxRetry) => {
        let retryCounter = 0;
        const batchWrite = async () => {
            try {
                await Promise.all(Items.map(async (Item, index) => {
                    try {
                        await this.DDB.put({TableName, Item, Expected: {[uniqueKey]: {Exists: false}}}).promise()
                    } catch (e) {
                        if(e.message === "The conditional request failed") {
                            throw new Error("Secure Put failed, items already exists - " + (index > 0 ? "Some items were already written" : "No items written"))
                        }
                    }
                }))
            } catch (e) {
                if(retryCounter < (maxRetry || 2)) {
                    retryCounter ++
                    //console.log("Retried: " + retryCounter)
                    if(e.message === "Secure Put failed, items already exists - No items written") {
                        await batchWrite()
                    } else {
                        throw Error("Secure Put Failed. Retry failed.")
                    }
                } else {
                    throw Error("Secure Put Failed. Retry failed.")
                }
            }
        }
        return await batchWrite()
       
    }
    batchDelete = async (TableName, Keys) => await Promise.all(Keys.chunk(25).map(chunk => this.DDB.batchWrite({
        RequestItems: {[TableName]: chunk.map(Key => ({DeleteRequest: {Key}}))}}).promise()
    ))
    batchUpdate = async (TableName, Items, Settings) => {
        return await Promise.all(Items.map(Item => {
            let params = {
                TableName,
                Key: Item.key
            };

            if(Item.Expected) params.Expected = {
                ComparisonOperator: Item.Expected.operator,
                AttributeValueList: Array.isArray(Item.Expected.value) ? [Item.Expected.value] : [[Item.Expected.value]]
            }

            let addOperations = [];
            let setOperations = [];
            let deleteOperations = [];
            let removeOperations = [];
            let attributeNames = {};
            let attributeValues = {};
            let conditionExpressions = [];

            Object.keys(Item.updateAttributes).forEach(key => {
                    if(Item.updateAttributes[key] || typeof Item.updateAttributes[key] == "boolean") {
                        if(typeof Item.updateAttributes[key] === "object" && Item.updateAttributes[key].action) {
                            if(Item.updateAttributes[key].action === "ADD") {
                               // if is nested
                                if(key.indexOf(".") >= 0) {
                                   throw Error("Update operation ADD not supported for nested attributes");
                                } else {
                                    addOperations.push("#" + key + "Key " + ":" + key + "Value");
                                    attributeNames["#" + key + "Key"] = key;
                                    attributeValues[":" + key.replace(".", "") + "Value"] = Item.updateAttributes[key].value;
                                }
                            } else if (Item.updateAttributes[key].action === "SET") {
                                // if is nested
                                const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                                if(key.indexOf(".") >= 0) {
                                    let keyString = "";
                                    const keys = key.split(".");
                                    keys.forEach((keyforkeyString, index) => {
                                        keyString += "#" + undottedkey + keyforkeyString + "Key";
                                        if(index != keys.length - 1) {
                                            keyString += ".";
                                        }
                                    })
                                    keyString = keyString.replace(/\-/g, "_")
                                    setOperations.push(keyString + " = " + ":" + undottedkey + "Value");
                                    keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"]= keyForExpression);
                                    attributeValues[":" + undottedkey + "Value"] = Item.updateAttributes[key].value;
                                } else {
                                    setOperations.push("#" + key + "Key = " + ":" + undottedkey + "Value");
                                    attributeNames["#" + key + "Key"] = key;
                                    attributeValues[":" + undottedkey + "Value"] = Item.updateAttributes[key].value;
                                }
                            } else if (Item.updateAttributes[key].action === "DELETE") {
                                throw Error("Update operation DELETE not supported");
                            } else if (Item.updateAttributes[key].action === "REMOVE") {
                                 // if is nested
                                if(key.indexOf(".") >= 0) {
                                   throw Error("Update operation REMOVE not supported for nested attributes");
                                } else {
                                    removeOperations.push(key);
                                }
                            } else if (Item.updateAttributes[key].action === "appendArray") {
                                // if is nested
                                const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                                if(key.indexOf(".") >= 0) {
                                    let keyString = "";
                                    const keys = key.split(".");
                                    keys.forEach((keyforkeyString, index) => {
                                        keyString += "#" + undottedkey + keyforkeyString + "Key";
                                        if(index != keys.length - 1) {
                                            keyString += ".";
                                        }
                                    })
                                    keyString = keyString.replace(/\-/g, "_")
                                    setOperations.push(keyString + " = list_append(if_not_exists(" + keyString + ", :empty_list),:" + undottedkey + "Value" + ")");
                                    keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"]= keyForExpression);
                                    attributeValues[":" + undottedkey + "Value"] = Item.updateAttributes[key].value;
                                    attributeValues[":empty_list"] = [];
                                } else {
                                    setOperations.push("#" + key + "Key = list_append(if_not_exists("+key +"Key, :empty_list),:" + undottedkey + "Value" + ")");
                                    attributeNames["#" + key + "Key"] = key;
                                    attributeValues[":" + undottedkey + "Value"] = Item.updateAttributes[key].value;
                                    attributeValues[":empty_list"] = [];
                                }
                             }
                             else if (Item.updateAttributes[key].action === "removeFromArray") {
                                // if is nested
                                const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                                if(key.indexOf(".") >= 0) {
                                    let keyString = "";
                                    const keys = key.split(".");
                                    keys.forEach((keyforkeyString, index) => {
                                        keyString += "#" + undottedkey + keyforkeyString + "Key";
                                        if(index != keys.length - 1) {
                                            keyString += ".";
                                        }
                                    })
                                    keyString = keyString.replace(/\-/g, "_")
                                    removeOperations.push(keyString + "[" + Item.updateAttributes[key].index + "]");
                                    keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"]= keyForExpression);
                                    if(Item.updateAttributes[key].validations) {
                                        Item.updateAttributes[key].validations.forEach(validation => {
                                            if(validation.type === "equals") {
                                                conditionExpressions.push(keyString + "[" + Item.updateAttributes[key].index + "]" + ".#" + validation.key + " = :" + validation.value.replace(/\-/g, "_"))
                                                attributeNames["#" + validation.key] = validation.key
                                                attributeValues[":" + validation.value.replace(/\-/g, "_")] = validation.value
                                            }
                                            
                                        })
                                    }
                                } else {
                                    removeOperations.push("#" + key + "[" + UpdateAttributes[key].index + "]");
                                    attributeNames["#" + key] = key;
                                    if(UpdateAttributes[key].validations) {
                                        UpdateAttributes[key].validations.forEach(validation => {
                                            if(validation.type === "equals") {
                                                conditionExpressions.push("#" + key + "[" + UpdateAttributes[key].index + "]" + ".#" + validation.key + " = :" + (typeof validation.value === "string" ? validation.value.replace(/\-/g, "_") : validation.value))
                                                attributeNames["#" + validation.key] = validation.key
                                                attributeValues[":" + (typeof validation.value === "string" ? validation.value.replace(/\-/g, "_") : validation.value)] = validation.value
                                            }
                                            
                                        })
                                    }
                                }
                             }
                        } else {
                                const undottedkey = key.replace(/\./g, "").replace(/\-/g, "_");
                                if(key.indexOf(".") >= 0) {
                                    let keyString = "";
                                    const keys = key.split(".");
                                    keys.forEach((keyforkeyString, index) => {
                                        keyString += "#" + undottedkey + keyforkeyString  + "Key";
                                        if(index != keys.length - 1) {
                                            keyString += ".";
                                        }
                                    })
                                    keyString = keyString.replace(/\-/g, "_")
                                    setOperations.push(keyString + " = " + ":" + undottedkey + "Value");
                                    keys.forEach(keyForExpression => attributeNames["#" + undottedkey + keyForExpression.replace(/\-/g, "_") + "Key"] = keyForExpression)
                                    attributeValues[":" + undottedkey + "Value"] = Item.updateAttributes[key]
                                } else {
                                    setOperations.push("#" + key + "Key = " + ":" + undottedkey + "Value");
                                    attributeNames["#" + key + "Key"] = key;
                                    attributeValues[":" + undottedkey + "Value"] = Item.updateAttributes[key];
                                }
                        }
                    }
                }
            );
            let expressionsArr = []

            if(addOperations.length > 0) expressionsArr.push("ADD " + addOperations.join(", ")); 
            if(setOperations.length > 0) expressionsArr.push("SET " + setOperations.join(", ")); 
            if(deleteOperations.length > 0) expressionsArr.push("DELETE " + deleteOperations.join(", ")); 
            if(removeOperations.length > 0) expressionsArr.push("REMOVE " + removeOperations.join(", "));
            if(conditionExpressions.length > 0) params.ConditionExpression = conditionExpressions.join(" AND ")
            params.UpdateExpression = expressionsArr.join(" ");
            params.ExpressionAttributeNames = attributeNames;
            params.ExpressionAttributeValues = attributeValues;
            // console.log("addOperations", addOperations);
            // console.log("setOperations", setOperations);
            // console.log("deleteOperations", deleteOperations);
            // console.log("removeOperations", removeOperations);
            // console.log("attributeNames", attributeNames);
            // console.log("attributeValues", attributeValues);
            // console.log("expression", expressionsArr.join(" "))
            //console.log(params)
            return this.DDB.update(params).promise();
        }))
    }
    query = async (TableName, primaryKey, settings) => {
        let params = {
            TableName,
            KeyConditions: {}
        }
        Object.keys(primaryKey).forEach(key => (typeof primaryKey[key] === "object" && Object.keys(key).length > 0) ?
            params.KeyConditions[key] = {
                ComparisonOperator: primaryKey[key].operator, 
                AttributeValueList: Array.isArray(primaryKey[key].value) ? primaryKey[key].value : [primaryKey[key].value]
            }
            :
            params.KeyConditions[key] = {ComparisonOperator: "EQ", AttributeValueList: [primaryKey[key]]}
        );
        if(settings) {
            if(settings.index) params.IndexName = settings.index;
            if(settings.limit) params.Limit = settings.limit;
            if(settings.startKey) params.ExclusiveStartKey = settings.startKey;
            if(settings.forward) params.ScanIndexForward = settings.forward;
            if(settings.attributes) params.AttributesToGet = settings.attributes;
            if(settings.filter) {
                params.QueryFilter = {};
                Object.keys(settings.filter).forEach(key => (typeof primaryKey[key] === "object" && Object.keys(key).length > 0) ?
                    params.QueryFilter[key] = {
                        ComparisonOperator: settings.filter[key].operator, 
                        AttributeValueList: Array.isArray(settings.filter[key].value) ? settings.filter[key].value : [settings.filter[key].value]
                    }
                    :
                    params.QueryFilter[key] = {ComparisonOperator: "EQ", AttributeValueList: [settings.filter[key]]}
                );
            }
        }
        // Make Request
        let res = []
        let resCount = 0
        const request = async (params) => {
            const requestRes = await this.DDB.query(params).promise()
            res.push(requestRes.Items)
            resCount += requestRes.Count
            if(requestRes.LastEvaluatedKey && (!settings || !settings.limit || settings.limit && (requestRes.Count + resCount < settings.limit))) {
                await request({...params, ExclusiveStartKey: requestRes.LastEvaluatedKey})
            }
        }
        await request(params)
        return res.flatten()
    }
    scan = async (TableName, scanKeys, settings) => {
        let params = {
            TableName
        }
        if(scanKeys) {
            params.ScanFilter = {}
            Object.keys(scanKeys).forEach(key => (typeof key === "object" && Object.keys(key).length > 0) ?
                params.ScanFilter[key] = {
                    ComparisonOperator: scanKeys[key].operator, 
                    AttributeValueList: Array.isArray(scanKeys[key].value) ? [scanKeys[key].value] : [[scanKeys[key].value]]
                }
                :
                params.ScanFilter[key] = {ComparisonOperator: "EQ", AttributeValueList: [scanKeys[key]]}
            );
        }
        if(settings) {
            if(settings.index) params.IndexName = settings.index;
            if(settings.limit) params.Limit = settings.limit;
            if(settings.startKey) params.ExclusiveStartKey = settings.startKey;
            if(settings.condition) params.ConditionalOperator = settings.condition;
            if(settings.attributes) params.AttributesToGet = settings.attributes;
            if(settings.consistent) params.ConsistentRead = settings.consistent;
        }
        // Make Request
        let res = []
        let resCount = 0
        const request = async (params) => {
            const requestRes = await this.DDB.scan(params).promise()
            res.push(requestRes.Items)
            resCount += requestRes.Count
            if(requestRes.LastEvaluatedKey && (!settings || !settings.limit || settings.limit && (requestRes.Count + resCount < settings.limit))) {
                await request({...params, ExclusiveStartKey: requestRes.LastEvaluatedKey})
            }
        }
        await request(params)
        return res.flatten()
    }
    nextId = async (TableName, identifier, partitionKey) => {
        const params = {
            TableName,
            Limit: 1,
            ScanIndexForward: false,
            KeyConditions: {
                [partitionKey["name"]]: {
                    ComparisonOperator: "EQ",
                    AttributeValueList: [partitionKey["value"]]
                }
            }
        };
        return this.DDB.query(params).promise()
            .then(r => {
                if (r.Items.length > 0) {
                    return r.Items[0][identifier] + 1;
                }
                else {
                    return 1;
                }
            });
    }
}