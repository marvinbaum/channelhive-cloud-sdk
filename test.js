const connect = new (require("./index"))({platform: "aws", region: "eu-fra"});


(async () => {
    try {
        // const res = await connect.database.batchGet("Connect_Products", [{owner:"tester", id: 7}, {owner:"tester", id: 8}]);
        //await connect.store.put("test.json", "lalala", "corona-2019-data")
        await connect.database.batchUpdate("Connect_Products", [
            {
                key: {
                    account: "softskin",
                    id: 2222
                },
                updateAttributes: {
                    "invoice.Amount": "Deniz",
                    "tax": {
                        action: "ADD",
                        value: 5
                    }
                }
            }
        ]);
    } catch (e) {
        console.log(e);
    }
})();
