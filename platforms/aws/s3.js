const S3Client = require('aws-sdk/clients/s3');

module.exports = class S3 {
    constructor() {
        this.S3 = new S3Client();
    }
    put = async (Key, Body, Bucket, ContentEncoding) => this.S3.putObject({Key, Body, Bucket, ContentEncoding}).promise()

    upload = async (Bucket, Key, Body, Settings) => this.S3.upload({Bucket, Key, Body}).promise()

    get = async (Bucket, Key) => this.S3.getObject({Bucket, Key}).promise().then(r => r.Body)
    
}