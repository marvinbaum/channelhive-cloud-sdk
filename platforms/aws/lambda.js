const Lambda = require('aws-sdk/clients/lambda');

module.exports = class lambda {
    constructor(region) {
        this.LAM = new Lambda({region});
    }
    invoke = async (FunctionName, Payload, InvocationType) => {
        const newPayload = typeof Payload === "object" ? JSON.stringify(Payload) : Payload;
        return this.LAM.invoke({FunctionName, InvocationType: InvocationType || "RequestResponse", Payload: newPayload}).promise()
        .then(r => {
            if(r.Payload) {
                return typeof r.Payload === "object" ? r.Payload : JSON.parse(r.Payload);
            } else {
                return typeof r === "object" ? r : JSON.parse(r);
            }
        })
    }
    invokeAsync = async (FunctionName, InvokeArgs) => this.LAM.invokeAsync({FunctionName, InvokeArgs}).promise()
    
}